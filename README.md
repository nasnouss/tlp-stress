


# Build the project 


```
./gradlew shadowJar
```

# Docker 

## login to docker hub 
```
docker login
```


## Build and push 
```
docker build --tag pns-stress-dev .
docker tag pns-stress-dev:latest <REPO_NAME>:<NEW_VERSION> 
docker push <REPO_NAME>:<NEW_VERSION>
```

# Customz class PIG 


A new table is created in the keyspace tlp_stress : 

```
CREATE TABLE tlp_stress.pig (
    kvalue text,
    ktype text,
    prov text,
    dname text,
    dattrib map<text, text>,
    dvalue text,
    PRIMARY KEY (kvalue, ktype, prov, dname)
) 
```

the class is locate in the foloowig folder : ./src/main/kotlin/com/thelastpickle/tlpstress/profiles/PigTable.kt

exemple to launch a test : 
```

docker run nasnouss/pns:latest run PigTable -i 1K -p 5k -r .5 --host <CASSANDRA_HOST>
```



