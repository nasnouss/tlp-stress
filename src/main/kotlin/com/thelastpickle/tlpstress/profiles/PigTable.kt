package com.thelastpickle.tlpstress.profiles

import com.datastax.driver.core.PreparedStatement
import com.datastax.driver.core.Session
import com.thelastpickle.tlpstress.PartitionKey
import com.thelastpickle.tlpstress.StressContext
import com.thelastpickle.tlpstress.generators.Field
import com.thelastpickle.tlpstress.generators.FieldFactory
import com.thelastpickle.tlpstress.generators.FieldGenerator
import com.thelastpickle.tlpstress.generators.functions.Random
import io.github.serpro69.kfaker.Faker


class PigTable : IStressProfile {

    lateinit var insert: PreparedStatement
    lateinit var select: PreparedStatement
    lateinit var delete: PreparedStatement






    override fun prepare(session: Session) {
        insert = session.prepare("INSERT INTO pig (kvalue, ktype, prov, dname, dattrib, dvalue) VALUES (?, ?, 'PRV', 'stringfield', ?, ?)")
        select = session.prepare("SELECT * from pig WHERE kvalue = ?")
        delete = session.prepare("DELETE from pig WHERE kvalue = ?")
    }

    override fun schema(): List<String> {
        val table = """CREATE TABLE IF NOT EXISTS pig (
                        kvalue text,
                        ktype text,
                        prov text,
                        dname text,
                        dattrib map<text, text>,
                        dvalue text,
                        PRIMARY KEY (kvalue, ktype, prov, dname)
                        )""".trimIndent()
        return listOf(table)
    }

    override fun getDefaultReadRate(): Double {
        return 0.5
    }

    override fun getRunner(context: StressContext): IStressRunner {


        return object : IStressRunner {

            override fun getNextSelect(partitionKey: PartitionKey): Operation {
                val bound = select.bind(partitionKey.getText())
                return Operation.SelectStatement(bound)
            }

            override fun getNextMutation(partitionKey: PartitionKey): Operation {
                val faker = Faker()

                val items = HashMap<String, String>()
                val dvalue =  faker.lorem.words()

                items["key1"] = "value1"
                items["key2"] = "value2"
                items["key3"] = "value3"

                val bound = insert.bind(partitionKey.getText(), "MSISDN", items, dvalue)

                return Operation.Mutation(bound)
            }

            override fun getNextDelete(partitionKey: PartitionKey): Operation {
                val bound = delete.bind(partitionKey.getText())
                return Operation.Deletion(bound)
            }
        }
    }

    override fun getFieldGenerators(): Map<Field, FieldGenerator> {
        val kv = FieldFactory("keyvalue")
        return mapOf(kv.getField("value") to Random().apply{min=100; max=200})
    }


}