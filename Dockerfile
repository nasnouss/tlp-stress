FROM openjdk:8-alpine3.7
WORKDIR /app
COPY build/ build
COPY .gradle .gradle 
COPY gradle/ gradle 
COPY src/  src
COPY bin/  bin


ENV WAIT_VERSION 2.7.2
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/$WAIT_VERSION/wait /wait
RUN chmod +x /wait

ENTRYPOINT ["sh", "./bin/tlp-stress"]
